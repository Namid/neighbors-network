const express = require('express');
const bodyParser = require('body-parser');

//Import routes
const authenticationRoutes = require('./api/routes/authentication');

var app = express();

//Database Connection


//Init Middleware
app.use(bodyParser.json());

//Import routes from api/routes
app.use("/api", authenticationRoutes)

app.listen(process.env.PORT || 5000, () => {
	console.log("Listening on Port 5000...");
});
