import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
var globalVar = 1;

function testFunc(){
  globalVar+=1;
  alert(globalVar);
}

export default function App() {
  let buttonTitle = "Test Button";
  const change = () => buttonTitle="change";
  return (
    <View style={styles.container}>
      <Button
          title={buttonTitle}
          onPress={testFunc}
        />
        <Text>{globalVar}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
